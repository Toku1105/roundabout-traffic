class Info {
  PVector position;
  Agent agent;
  
  int line, spacing = 14;

  Info(PVector position, Agent agent) {
    this.agent    = agent;
    this.position = position;
  }

  void draw(boolean active) {
    fill(255);
    if (!active) return;
    line = 0;
    printText("Pose: " + agent.position_wc.x + " " + agent.position_wc.y );
    printText("Phi: " + degrees(agent.headingAngle)+" Yaw: "+ degrees(agent.steerAngle));
    printText("KM/H: " + agent.car.getKilometersPerHour() + " T: " + agent.car.throttle);
    printText("RPM " + agent.engine.lastRPM + " G: " +agent.engine.currentGear);
    printText("Torque " + agent.car.cartype.axleRear.getTorque());
    printText("Angular V " + agent.car.angularvelocity);
    printText("TR slip " + agent.car.cartype.axleRear.slipAngle);
    printText("TF slip " + agent.car.cartype.axleFront.slipAngle);
    printText("Accel: " + agent.acceleration.x +"  " +agent.acceleration.y);
    printText("velocity: "+ agent.velocity.x + " " + agent.velocity.y);
   printText("velocity_wc: "+ agent.velocity_wc.x + " " + agent.velocity_wc.y);
   printText("accel_wc: "+ agent.acceleration_wc.mag()*agent.SGN(agent.acceleration_wc.heading()));
   printText("Weight_FR: "+ agent.car.cartype.axleFront.tireRight.activeWeight + " Wegiht_FL: "+agent.car.cartype.axleFront.tireLeft.activeWeight);
   printText("Weight_RR: "+ agent.car.cartype.axleRear.tireRight.activeWeight + " Wegiht_RL: "+agent.car.cartype.axleRear.tireLeft.activeWeight);
   
    line++;
    }

  void printText(String str) {
    text(str, position.x, position.y + line * spacing);
    line++;
  }
}
